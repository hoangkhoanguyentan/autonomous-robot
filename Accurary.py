import json
import numpy as np
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score

# Đọc dữ liệu từ file JSON
with open("HybridNets/regresstion/data2.json", "r") as file:
    json_data = json.load(file)

# Chuẩn bị dữ liệu
ultrasonics = [list(data['ultrasonic'].values()) for data in json_data]
segments = [data['segment'] for data in json_data]
speed_left = np.array([data['speed']['left'] for data in json_data])
speed_right = np.array([data['speed']['right'] for data in json_data])

# Dữ liệu mới
new_ultrasonic = [18, 12, 15]
new_segment = [
    [0, 1, 1, 1],
    [1, 0, 1, 1],
    [1, 0, 1, 0],
    [0, 0, 1, 1]
]

# Thêm dữ liệu mới vào tập dữ liệu hiện có
ultrasonics.append(new_ultrasonic)
segments.append(new_segment)
speed_left = np.append(speed_left, 0)  # Thêm nhãn tương ứng với dữ liệu mới
speed_right = np.append(speed_right, 0)  # Thêm nhãn tương ứng với dữ liệu mới

# Chuyển đổi dữ liệu segment thành ma trận 2D
segments = np.array(segments).reshape(len(segments), -1)

# Chuyển đổi dữ liệu thành mảng numpy
ultrasonics = np.array(ultrasonics)

# Chia dữ liệu thành tập huấn luyện và tập kiểm tra
X_train, X_test, y_train_left, y_test_left, y_train_right, y_test_right = train_test_split(
    segments, speed_left, speed_right, test_size=0.2, random_state=42
)

# Xây dựng và huấn luyện mô hình Random Forest cho tốc độ 'speed left'
model_left = RandomForestRegressor(n_estimators=100, random_state=42)
model_left.fit(X_train, y_train_left)
y_pred_left = model_left.predict(X_test)

# Xây dựng và huấn luyện mô hình Random Forest cho tốc độ 'speed right'
model_right = RandomForestRegressor(n_estimators=100, random_state=42)
model_right.fit(X_train, y_train_right)
y_pred_right = model_right.predict(X_test)

# Tính toán tỉ lệ chính xác (R2 score) trên tập kiểm tra
accuracy_left = r2_score(y_test_left, y_pred_left)
accuracy_right = r2_score(y_test_right, y_pred_right)

print("Accuracy (Speed Left):", accuracy_left)
print("Accuracy (Speed Right):", accuracy_right)