import json
import os
import cv2
from HybridNets.detect_model import HybridInference

class MakeLabel:
    def __init__(self, result_dir, out_name, id_camera):
        self.result_dir = result_dir
        self.out_name = out_name
        self.id_camera = id_camera
        self.result_path = os.path.join(self.result_dir, self.out_name)
        

    def get_param_from_hardware(self):
        # Code đọc phần cứng e bỏ vô đây và gán vào biến
        speed = {
            'left': 50,
            'right': 50,
        }
        ultrasonic = {
            'sonic_1': 50,
            'sonic_2': 50,
            'sonic_3': 50,
        }
        return speed, ultrasonic

    def run_video(self):
        results, count = [], 0
        detect = HybridInference()
        camera = cv2.VideoCapture(self.id_camera)

        try:
            with open(self.result_path, 'r') as g:
                results = json.load(g)
        except:
            results = []

        print("Running your camera ...")
        while True:
            _, img = camera.read()
            img = cv2.resize(img, (1280, 720))
            
            speed, ultrasonic = self.get_param_from_hardware()
            result = detect.export_json(img, speed, ultrasonic)
            results.append(result)
            print(count)
            count += 1

            cv2.imshow("Labeling dataset", img)
            k = cv2.waitKey(1)
            if k == ord('q') or k == 27:
                break

        cv2.destroyAllWindows()

        with open(self.result_path, 'w') as g:
            json.dump(results, g, indent=4)

        print(f"Overall dataset: [{count}]")
        print(f"Completed labeling and wrote data to [{self.result_path}]")

