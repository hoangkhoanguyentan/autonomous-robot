import json
import os

import cv2

from detect_model_test import HybridInference


def get_param_from_hardware():
    # Code đọc phần cứng e bỏ vô đây và gán vào biến
    speed = {
        'left': 50,
        'right': 50,
    }
    ultrasonic = {
        'sonic_1': 50,
        'sonic_2': 50,
        'sonic_3': 50,
    }
    return speed, ultrasonic


def run_video(id_camera, result_path):
    results, count = [], 0
    
    detect = HybridInference()
    camera = cv2.VideoCapture(id_camera)
    
    print("Running your camera ...")
    while True:
        _, img = camera.read()
        img = cv2.resize(img, (640, 360))
        
        speed, ultrasonic = get_param_from_hardware()
        result = detect.export_json(img, speed, ultrasonic)
        results.append(result)
        print(count)
        count += 1
        
        cv2.imshow("Labeling dataset", img)
        k = cv2.waitKey(1)
        if k == ord('q') or k == 27:
            break
    cv2.destroyAllWindows()
    
    with open(result_path, 'w') as g:
        json.dump(results, g, indent=4)
    print(f"Overall dataset: [{count}]")
    print(f"Completed labeing and write data to [{result_path}]")
        

if __name__ == '__main__':
    # Thay đổi path đường dẫn tới nơi lưu file e mong muôn
    result_dir = 'E:/'
    out_name = 'labeled_results.json'
    result_path = os.path.join(result_dir, out_name)
    
    # Set id_camera tức tracking url để thay camera của e
    id_camera = 0
    
    run_video(id_camera, result_path)
