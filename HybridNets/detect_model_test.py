import cv2
import numpy as np
import torch
from torchvision import transforms

from backbone import HybridNetsBackbone
from utils.utils import letterbox, BBoxTransform, ClipBoxes, postprocess

COMPOUND_COEF = 3
OBJ_LIST = ['car']
ANCHORS_SCALES = [2**0, 2**0.70, 2**1.32]
ANCHORS_RATIOS = [(0.62, 1.58), (1.0, 1.0), (1.58, 0.62)]
SEG_LIST = ['road', 'lane']
SEG_MODE = 'multiclass'
RESIZED_SHAPE = 640
COLOR_SEG = {
    'background': np.array([0, 0, 0]),  # Black
    'road': np.array([255, 255, 255]),  # White
    'lane': np.array([127, 127, 127]),  # Gray
}
THRESHOLD = 0.25
IOU_THRESHOLD = 0.3


class HybridInference:
    def __init__(
        self, device='cuda', weight_path='weights/hybridnets.pth', img_resize=(450, 300)
    ):
        self.device = self.verify_device(device)

        self.model = self._load_model(weight_path)
        self.regressBoxes = BBoxTransform()
        self.clipBoxes = ClipBoxes()
        self.transform = self.get_tfms()

        self.img_resize = img_resize
                
    def _load_model(self, weight_path):
        print(f"Loading model from [{weight_path}]...")
        model = HybridNetsBackbone(
            compound_coef=COMPOUND_COEF,
            num_classes=len(OBJ_LIST),
            ratios=ANCHORS_RATIOS,
            scales=ANCHORS_SCALES,
            seg_classes=len(SEG_LIST),
            backbone_name=None,
            seg_mode=SEG_MODE,
        )
        model_state = torch.load(weight_path, map_location=self.device)
        model.load_state_dict(model_state)

        model.requires_grad_(False)
        model.eval()
        print("Model load successful")   
        return model

    def predict(self, input):
        img = self._preprocess(input)
        _, _, _, _, seg = self.model(img)
        # raw_bbox = postprocess(
        #     img,
        #     anchors,
        #     regression,
        #     classification,
        #     self.regressBoxes,
        #     self.clipBoxes,
        #     THRESHOLD,
        #     IOU_THRESHOLD,
        # )
        # bboxes = self._bbox_process()
        bboxes = None
        color_seg = self._segment_process(seg)
        return bboxes, color_seg

    def export_json(self, img: np.ndarray, speed: dict, ultrasonic: dict):
        result = dict()
        bboxes, seg_mask = self.predict(img)
        seg_mask = cv2.cvtColor(seg_mask, cv2.COLOR_BGR2GRAY)
        seg_mask = cv2.resize(seg_mask, (self.img_resize))

        result['speed'] = speed
        result['ultrasonic'] = ultrasonic
        result['segment'] = seg_mask.tolist()
        result['boxes'] = bboxes
        return result

    def _preprocess(self, input):
        if isinstance(input, str):
            img = cv2.imread(input, cv2.IMREAD_COLOR | cv2.IMREAD_IGNORE_ORIENTATION)
        elif isinstance(input, np.ndarray):
            img = input.copy()
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        h0, w0 = img.shape[:2]
        r = RESIZED_SHAPE / max(img.shape[:2])
        input_img = cv2.resize(
            img, (int(w0 * r), int(h0 * r)), interpolation=cv2.INTER_AREA
        )
        (input_img, _), _, _ = letterbox(
            (input_img, None), RESIZED_SHAPE, auto=True, scaleup=False
        )
        input_img = self.transform(input_img)
        input_img = input_img.unsqueeze(0)
        input_img.to(self.device)
        return input_img.to(torch.float32)
    
    def _bbox_process(self):
        pass

    def _segment_process(self, seg):
        _, seg_mask = torch.max(seg, 1)
        seg_mask = seg_mask.squeeze().cpu().numpy()
        color_seg = np.zeros((seg_mask.shape[0], seg_mask.shape[1], 3), dtype=np.uint8)
        for index, seg_class in enumerate(COLOR_SEG.keys()):
            color_seg[seg_mask == index] = COLOR_SEG[seg_class]
        color_seg = color_seg[..., ::-1]
        return color_seg

    @staticmethod
    def get_tfms():
        # imagenet normalize
        normalize = transforms.Normalize(
            mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]
        )
        transform = transforms.Compose(
            [
                transforms.ToTensor(),
                normalize,
            ]
        )
        return transform

    @staticmethod
    def convert_numpy(input):
        input_2d = input.squeeze(0)
        input_nograd = input_2d.detach()
        input_cpu = input_nograd.cpu()
        input_numpy = input_cpu.numpy()
        return input_numpy.astype(np.float32)

    @staticmethod
    def verify_device(device):
        if device != 'cpu' and torch.cuda.is_available():
            return device
        return 'cpu'


if __name__ == '__main__':
    img_path = 'E:/HybridNets_end2end/demo/image/1.jpg'
    img = cv2.imread(img_path)

    model = HybridInference(device='cpu', weight_path='weights/hybridnets.pth')

    # Result of json
    speed = {
        'left': 50,
        'right': 50,
    }
    ultrasonic = {
        'sonic_1': 50,
        'sonic_2': 50,
        'sonic_3': 50,
    }
    res_json = model.export_json(img, speed, ultrasonic)
    print(res_json)
    print(res_json['segment'].shape)

    # Result of mask
    color_seg = model.predict(img_path)
    cv2.imshow('imgs', res_json['segment'])
    cv2.waitKey(0)

    # TODO: Result of bbox
