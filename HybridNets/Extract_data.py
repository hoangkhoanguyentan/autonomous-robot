import cv2
import os

ROOT = os.getcwd()

name_dir = f'{ROOT}/final-data'
name_new_dir = f'{ROOT}/images'

name_classes = os.listdir(name_dir)

for name_class in name_classes:
	name_file = os.listdir(f'{name_dir}/{name_class}')

	target_dir = f'{name_new_dir}/{name_class}'
	os.makedirs(target_dir)
 
	cap = cv2.VideoCapture(0)
	count = 0
 
	while cap.isOpened():
		ret, frame = cap.read()

		if ret:
			if count % 12 == 0:
				cv2.imwrite(f'{target_dir}/{name_class}_{str(count)}.jpg', frame)
			count += 1
		else:
			break
	cap.release()