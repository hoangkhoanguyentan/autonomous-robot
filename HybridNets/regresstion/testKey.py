import pygame

# Khởi tạo pygame
pygame.init()

# Khởi tạo tay cầm Xbox
joystick = pygame.joystick.Joystick(0)
joystick.init()

# Kiểm tra số lượng phím
num_buttons = joystick.get_numbuttons()
print("Số lượng phím:", num_buttons)

# Vòng lặp chờ sự kiện
while True:
    for event in pygame.event.get():
        if event.type == pygame.JOYAXISMOTION:  # Trục di chuyển
            axis_index = event.axis
            axis_value = joystick.get_axis(axis_index)
            print("Trục", axis_index, "giá trị:", axis_value)
        elif event.type == pygame.JOYBUTTONDOWN:  # Nút được nhấn
            button_index = event.button
            button_value = joystick.get_button(button_index)
            print("Nút", button_index, "được nhấn. Giá trị:", button_value)
        elif event.type == pygame.JOYBUTTONUP:  # Nút được nhả ra
            button_index = event.button
            button_value = joystick.get_button(button_index)
            print("Nút", button_index, "được nhả ra. Giá trị:", button_value)
