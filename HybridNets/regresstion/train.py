import json
from sklearn.ensemble import RandomForestRegressor
from joblib import dump, load

class SpeedPredictor:
    def __init__(self, num_trees=90):
        self.random_forest_left = RandomForestRegressor(n_estimators=num_trees)
        self.random_forest_right = RandomForestRegressor(n_estimators=num_trees)
    
    def fit(self, features, labels_left, labels_right):
        self.random_forest_left.fit(features, labels_left)
        self.random_forest_right.fit(features, labels_right)
    
    def predict(self, input_data):
        predicted_speed_left = self.random_forest_left.predict([input_data])
        predicted_speed_right = self.random_forest_right.predict([input_data])
        return predicted_speed_left, predicted_speed_right
    
    def save_model(self, filename):
        dump((self.random_forest_left, self.random_forest_right), filename)
    
    def load_model(self, filename):
        self.random_forest_left, self.random_forest_right = load(filename)


with open("HybridNets/regresstion/data2.json", "r") as file:
    data = json.load(file)
# Sử dụng lớp SpeedPredictor
speed_predictor = SpeedPredictor()

# Bước 2: Chuẩn bị dữ liệu huấn luyện và nhãn
features = []
labels_left = []
labels_right = []

for sample in data:
    features.append(sample["dis_ultra"])
    labels_left.append(float(sample["speed"]["left"]))  # Nhãn speed left
    labels_right.append(float(sample["speed"]["right"]))  # Nhãn speed right

# Bước 3: Huấn luyện mô hình Random Forest
speed_predictor.fit(features, labels_left, labels_right)

# Bước 4: Lưu trữ mô hình đã được huấn luyện
speed_predictor.save_model("HybridNets/demo_result/data_result/peed_predictor_model_1.joblib")