import random

# Hàm tạo mẫu dữ liệu ngẫu nhiên
def generate_random_sample():
    speed_left = random.uniform(300, 380)  # Tùy chỉnh phạm vi dựa trên nhu cầu
    speed_right = random.uniform(420, 500)
    dis_ultra = [
        f"{random.uniform(150, 180)}",    #xA
        f"{random.uniform(70, 90)}",    #yA
        f"{random.uniform(70, 700)}",
        f"{random.uniform(200, 250)}",    #xB
        f"{random.uniform(100, 130)}",    #yB
        f"{random.uniform(100, 700)}",
        f"{random.uniform(10, 40)}",    #xC
        f"{random.uniform(160, 180)}",    #yC
        f"{random.uniform(80, 700)}",
        f"{random.uniform(280, 320)}",    #xD
        f"{random.uniform(150, 180)}",    #yD
        f"{random.uniform(0.7, 1)}"
    ]
    
    sample = {
        "speed": {
            "left": speed_left,
            "right": speed_right
        },
        "dis_ultra": dis_ultra
    }
    
    return sample

# Tạo 10,000 mẫu ngẫu nhiên
samples = [generate_random_sample() for _ in range(5000)]

# In ra một số mẫu đầu tiên
for i in range(10):
    print(samples[i])

# Lưu mẫu dữ liệu vào một tệp JSON (tuỳ chọn)
import json

with open("HybridNets/regresstion/data3.json", "w") as f:
    json.dump(samples, f)