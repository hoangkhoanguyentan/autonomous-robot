import json
import numpy as np
from sklearn.tree import DecisionTreeRegressor

# Đọc dữ liệu từ tệp JSON
with open('HybridNets/regresstion/data2.json', 'r') as file:
    data = json.load(file)

# Lấy dữ liệu huấn luyện
train_data = data

# Tạo mảng numpy chứa dữ liệu huấn luyện
ultrasonic_values = [item.get('ultrasonic', {}) for item in train_data]
segment_values = [item.get('segment', []) for item in train_data]

X_train = np.concatenate(
    (np.array([list(ultrasonic.values()) for ultrasonic in ultrasonic_values]),
     np.array(segment_values).reshape(-1, 16)),
    axis=1
)
y_train_left = np.array([item.get('speed', {}).get('left', 0) for item in train_data])
y_train_right = np.array([item.get('speed', {}).get('right', 0) for item in train_data])

# Dữ liệu đầu vào mới cần dự đoán
new_ultrasonic = {
    "sonic_1": 12,
    "sonic_2": 17,
    "sonic_3": 19
}
new_segment = [
    [0, 1, 0, 0],
    [0, 1, 1, 1],
    [1, 0, 0, 1],
    [1, 0, 0, 0]
]
X_new = np.concatenate((np.array(list(new_ultrasonic.values())), np.array(new_segment).flatten())).reshape(1, -1)

# Tạo và huấn luyện mô hình Decision Tree cho speed left
model_left = DecisionTreeRegressor()
model_left.fit(X_train, y_train_left)

# Tạo và huấn luyện mô hình Decision Tree cho speed right
model_right = DecisionTreeRegressor()
model_right.fit(X_train, y_train_right)

# Dự đoán speed left và speed right cho dữ liệu mới
predicted_speed_left = model_left.predict(X_new)
predicted_speed_right = model_right.predict(X_new)

# In kết quả
print('Predicted Speed (Left):', predicted_speed_left[0])
print('Predicted Speed (Right):', predicted_speed_right[0])
