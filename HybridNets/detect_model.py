import math
import time
import cv2
import numpy as np
import torch
from torchvision import transforms

from HybridNets.backbone import HybridNetsBackbone
from HybridNets.utils.utils import letterbox, BBoxTransform, ClipBoxes, postprocess

COMPOUND_COEF = 3
OBJ_LIST = ['car']
ANCHORS_SCALES = [2**0, 2**0.70, 2**1.32]
ANCHORS_RATIOS = [(0.62, 1.58), (1.0, 1.0), (1.58, 0.62)]
SEG_LIST = ['road', 'lane']
SEG_MODE = 'multiclass'
RESIZED_SHAPE = 640
COLOR_SEG = {
    'background': np.array([0, 0, 0]),  # Black
    'road': np.array([255, 255, 255]),  # White
    'lane': np.array([127, 127, 127]),  # Gray
}
THRESHOLD = 0.25
IOU_THRESHOLD = 0.3


class HybridInference:
    def __init__(
        self, device='cuda', weight_path='HybridNets/weights/hybridnets.pth', img_resize=(320, 180)
    ):
        self.device = self.verify_device(device)
        
        self.model = self._load_model(weight_path)
        self.regressBoxes = BBoxTransform()
        self.clipBoxes = ClipBoxes()
        self.transform = self.get_tfms()
        # self.seg_mask = None
        self.img_resize = img_resize
        self.distances = []
                
    def _load_model(self, weight_path):
        print(f"Loading model from [{weight_path}]...")
        model = HybridNetsBackbone(
            compound_coef=COMPOUND_COEF,
            num_classes=len(OBJ_LIST),
            ratios=ANCHORS_RATIOS,
            scales=ANCHORS_SCALES,
            seg_classes=len(SEG_LIST),
            backbone_name=None,
            seg_mode=SEG_MODE,
        )
        model_state = torch.load(weight_path, map_location=self.device)
        model.load_state_dict(model_state)

        model.requires_grad_(False)
        model.to(self.device)
        model.eval()
        print("Model load successful")   
        return model

    def predict(self, input):
        
        img = self._preprocess(input)
        _, _, _, _, seg = self.model(img.cuda())
        bboxes = None
        color_seg = self._segment_process(seg)
        return bboxes, color_seg
    def limit_keypoints(self, keypoints, descriptors, num_points, center):
        angles = [np.arctan2(kp.pt[1] - center[1], kp.pt[0] - center[0]) for kp in keypoints]
        sorted_indices = np.argsort(angles)
        keypoint_with_min_y = min(keypoints, key=lambda kp: kp.pt[1])
        sorted_x = sorted(keypoints, key=lambda kp: kp.pt[0])
        sorted_x_reversed = sorted_x[::-1]
        best_keypoints = []
        for kp in sorted_x:
            if kp.pt[1] - keypoint_with_min_y.pt[1] < 19:
                best_keypoints.append(kp)
                break
        for kp in sorted_x_reversed:
            if kp.pt[1] - keypoint_with_min_y.pt[1] < 19:
                best_keypoints.append(kp)
                break
        best_indices = [sorted_indices[0], sorted_indices[-1]]
        best_keypoints += [keypoints[i] for i in best_indices]
        best_descriptors = descriptors[best_indices]
        x1 = self.compute_line_length(keypoints[sorted_indices[0]], center )
        x2 = self.compute_line_length(keypoints[sorted_indices[-1]], center )
        
        return best_keypoints, best_descriptors, x1/x2

    def compute_keypoint_score(self,keypoints):

        score = 0

        for kp1 in keypoints:
            for kp2 in keypoints:
                distance = np.linalg.norm(np.array(kp1.pt) - np.array(kp2.pt))
                score += distance

        return score

    def define_vector(self, img):
        
        sift = cv2.SIFT_create()
        start_point = (img.shape[1]//2, img.shape[0])
        # Tìm các điểm đặc trưng và mô tả của chúng
        keypoints_input, descriptors = sift.detectAndCompute(img, None)
        if len(keypoints_input) == 0:
            dis_off = [[0,0],[0,0],[0,0],[0,0]]
            
            
            return img, dis_off, 0
        else:
        # Giới hạn số lượng điểm đặc trưng xuống 4 điểm
            keypoints, descriptors, ratio = self.limit_keypoints(keypoints_input, descriptors, 4, start_point)
            distances = []
            # Vẽ các điểm đặc trưng trên hình ảnh
            image_with_keypoints = cv2.drawKeypoints(img, keypoints_input, None)
            for kp in keypoints:
                
                distance = [round(kp.pt[0],3),round(kp.pt[1],3)]
                distances.append(distance)
                pt1 = (int(start_point[0]), int(start_point[1]))
                pt2 = (int(kp.pt[0]), int(kp.pt[1]))
                cv2.line(image_with_keypoints, pt1, pt2, (0, 0, 255), 2)
        

            return image_with_keypoints, distances, ratio
    def compute_line_length(self, pt1, pt2):
            distance = math.sqrt((pt2[0] - pt1.pt[0]) ** 2 + (pt2[1] - pt1.pt[1]) ** 2)
            return round(distance,3)
        
    def export_json(self, distances, speed: dict, ultrasonic: dict):
        result = dict()
        self.distances = distances
        result['speed'] = speed
        result['dis_ultra'] = np.c_[distances,np.array(ultrasonic)].flatten().tolist()
        return result
    def VideoGray(self, seg_mask):
        return seg_mask

    def _preprocess(self, input):
        if isinstance(input, str):
            img = cv2.imread(input, cv2.IMREAD_COLOR | cv2.IMREAD_IGNORE_ORIENTATION)
        elif isinstance(input, np.ndarray):
            img = input.copy()
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        h0, w0 = img.shape[:2]
        r = RESIZED_SHAPE / max(img.shape[:2])
        input_img = cv2.resize(
            img, (int(w0 * r), int(h0 * r)), interpolation=cv2.INTER_AREA
        )
        (input_img, _), _, _ = letterbox(
            (input_img, None), RESIZED_SHAPE, auto=True, scaleup=False
        )
        input_img = self.transform(input_img)
        input_img = input_img.unsqueeze(0)
        input_img.to(self.device)
        return input_img.to(torch.float32)
    
    def _bbox_process(self):
        pass

    def _segment_process(self, seg):
        _, seg_mask = torch.max(seg, 1)
        seg_mask = seg_mask.squeeze().cpu().numpy()
        color_seg = np.zeros((seg_mask.shape[0], seg_mask.shape[1], 3), dtype=np.uint8)
        for index, seg_class in enumerate(COLOR_SEG.keys()):
            color_seg[seg_mask == index] = COLOR_SEG[seg_class]
        color_seg = color_seg[..., ::-1]
        return color_seg

    @staticmethod
    def get_tfms():
        # imagenet normalize
        normalize = transforms.Normalize(
            mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]
        )
        transform = transforms.Compose(
            [
                transforms.ToTensor(),
                normalize,
            ]
        )
        return transform

    @staticmethod
    def convert_numpy(input):
        input_2d = input.squeeze(0)
        input_nograd = input_2d.detach()
        input_cpu = input_nograd.cpu()
        input_numpy = input_cpu.numpy()
        return input_numpy.astype(np.float32)

    @staticmethod
    def verify_device(device):
        if device != 'cpu' and torch.cuda.is_available():
            return device
        return 'cpu'
