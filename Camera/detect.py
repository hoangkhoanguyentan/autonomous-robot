import json
import threading
import cv2
import numpy as np
from sklearn.ensemble import RandomForestRegressor

from HybridNets.detect_model import HybridInference
# from HybridNets.detect_model import HybridInference
# from queue import Queue


class DetectionThread(threading.Thread):
    def __init__(self,result_path,image_queue, main):
          threading.Thread.__init__(self)
          self.detect = HybridInference()
          self.image_queue = image_queue

          self.result_path = result_path
          self.distance_main = main

    def run(self):
        count = 0
        results = []
        try:
            with open(self.result_path, 'r') as f:
                results = json.load(f)
        except:
            results = []
        while True:
            img = self.image_queue.get()  # Lấy hình ảnh từ hàng đợi

            if img is None:
                break
            speed, ultrasonic = self.distance_main.get_param_from_hardware()
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            img = cv2.resize(img, (320,180))
            img = cv2.GaussianBlur(img, (7,7), 0)
            _, img = self.detect.predict(img)
            img, distances, ratio = self.detect.define_vector(img)
            ultrasonic.append(ratio)
            
            result = self.detect.export_json(distances, speed, ultrasonic)
            results.append(result)

        cv2.imshow("Labeling dataset", img)
            k = cv2.waitKey(1)
            if k == ord('q') or k == 27:
                break
            
        with open('HybridNets/demo_result/data_result/data.json', 'w') as file:
            json.dump(results, file, indent=4)

        cv2.destroyAllWindows()

