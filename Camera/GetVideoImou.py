from onvif import ONVIFCamera
import requests

# Thay thế các thông tin sau đây bằng thông tin của camera Imou của bạn
camera_ip = "192.168.1.171"
camera_port = 80
camera_username = "SVNDH-IPC-CX2E"
camera_password = "L2B274DC"

def capture_image():
    # Kết nối đến camera ONVIF
    camera = ONVIFCamera(camera_ip, camera_port, camera_username, camera_password)
    print(camera)
    # Lấy thông tin về media service
    media_service = camera.create_media_service()
    
	
    # Lấy thông tin về các profile hỗ trợ
    profiles = media_service.GetProfiles()

    # Lấy URL của hình ảnh snapshot
    snapshot_url = media_service.GetSnapshotUri(profiles[0].token).Uri

    # Tạo yêu cầu GET để lấy hình ảnh từ URL snapshot
    response = requests.get(snapshot_url, auth=(camera_username, camera_password), timeout=10)

    # Kiểm tra nếu yêu cầu thành công (mã trạng thái 200)
    if response.status_code == 200:
        # Lưu trữ hình ảnh vào file
        with open("image.jpg", "wb") as file:
            file.write(response.content)
        print("Đã chụp ảnh thành công!")
    else:
        print("Không thể kết nối đến camera.")

# Gọi hàm capture_image để lấy hình ảnh từ camera
capture_image()