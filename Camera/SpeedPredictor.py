from sklearn.ensemble import RandomForestRegressor
import joblib

class SpeedPredictor_RdForest:
    def __init__(self):
        self.speed_left_model = None
        self.speed_right_model = None
    
    def load_model(self, file_path):
        models = joblib.load(file_path)
        self.speed_left_model = models[0]
        self.speed_right_model = models[1]
    
    def predict(self, input_data):
        predicted_speed_left = self.speed_left_model.predict([input_data])
        predicted_speed_right = self.speed_right_model.predict([input_data])
        return predicted_speed_left[0], predicted_speed_right[0]