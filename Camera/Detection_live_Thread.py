import threading
import time
import cv2
import numpy as np
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error
from CameraImouLife.SpeedPredictor import SpeedPredictor_RdForest
from HybridNets.detect_model import HybridInference
import joblib
import json


class Detection_live_Thread(threading.Thread):
    def __init__(self, ultrasonic, image_queue, main_instance):
        threading.Thread.__init__(self)
        self.model = RandomForestRegressor()
        self.detect = HybridInference() 
        self.image_queue = image_queue
        self.ultrasonic = ultrasonic
        self.X_train = []
        self.y_train = []
        self.features = []
        self.left = 0
        self.right = 0
        self.speed_predictor = SpeedPredictor_RdForest()
        self.speed_predictor.load_model("HybridNets/demo_result/data_result/peed_predictor_model.joblib")
        self.main_instance = main_instance
        self.lock = threading.Lock()


    

    def run(self):
        while True:
            img = self.image_queue.get()  # Lấy hình ảnh từ hàng đợi

            if img is None:
                break

            _, ultrasonic = self.main_instance.get_param_from_hardware()
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            img = cv2.resize(img, (320, 180))
            img = cv2.GaussianBlur(img, (5, 5), 0)

            # Xử lý và dự đoán tốc độ

            _, img = self.detect.predict(img)
            img, distances, ratio = self.detect.define_vector(img)
            ultrasonic.append(ratio)
            # predicts = self.predict(ultrasonic, distances)
            # print(predicts)
            new_X = np.c_[np.array(distances),np.array(ultrasonic)].flatten()
            predicted_speed_left, predicted_speed_right = self.speed_predictor.predict(new_X)
            self.main_instance.left = predicted_speed_left
            self.main_instance.right = predicted_speed_right
            # Xử lý và hiển thị ảnh
            cv2.imshow("Labeling dataset", img)

            k = cv2.waitKey(1)
            if k == ord('q') or k == 27:
                break

        cv2.destroyAllWindows()