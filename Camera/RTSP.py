# import cv2

# # Địa chỉ RTSP của camera
# rtsp_url = 'rtsp://admin:sonk51ca1@192.168.1.171:554/cam/realmonitor?channel=1&subtype=1'
# # Tạo đối tượng VideoCapture với địa chỉ RTSP
# cap = cv2.VideoCapture(rtsp_url)

# # Kiểm tra xem việc kết nối đã thành công chưa
# if not cap.isOpened():
#     print("Không thể kết nối tới camera")
#     exit()

# while True:
#     ret, frame = cap.read()

#     if not ret:
#         print("Không thể đọc khung hình")
#         break

#     cv2.imshow("Live Video", frame)

#     if cv2.waitKey(1) == ord('q'):
#         break

# cap.release()
# cv2.destroyAllWindows()


import cv2
import json
import threading

from HybridNets.detect_model import HybridInference


class VideoThread(threading.Thread):
    def __init__(self, camera_id, image_queue):
        threading.Thread.__init__(self)
        self.camera_id = camera_id
        self.image_queue = image_queue
        self.MAX_QUEUE_SIZE  = 100

    def run(self):
        camera = cv2.VideoCapture(self.camera_id)
        count = 0
        while True:
            ret, img = camera.read()
            if not ret or img is None:
                break
            if count % 17 == 0:
                self.image_queue.put(img)  # Đưa hình ảnh vào hàng đợi
            count += 1
        camera.release()