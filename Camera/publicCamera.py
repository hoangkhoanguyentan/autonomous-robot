import requests
import cv2
import numpy as np

# Địa chỉ IP và cổng của camera Imou
camera_ip = '192.168.1.171'
camera_port = 80
camera_username = 'admin'
camera_password = 'sonk51ca1'

# Tạo URL để truy cập hình ảnh từ camera
url = f'http://{camera_ip}:{camera_port}/cgi-bin/snapshot.cgi'

# Tạo header chứa thông tin xác thực
auth = (camera_username, camera_password)

# Gửi yêu cầu GET để lấy hình ảnh từ camera
response = requests.get(url, auth=auth)
print(response.status_code)
# Kiểm tra xem yêu cầu có thành công hay không
if response.status_code == 200:
    # Chuyển đổi dữ liệu nhận được thành mảng numpy
    img_array = np.array(bytearray(response.content), dtype=np.uint8)
    # Đọc hình ảnh từ mảng numpy
    img = cv2.imdecode(img_array, cv2.IMREAD_COLOR)
    # Hiển thị hình ảnh
    cv2.imshow('Camera Image', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
else:
    print('Không thể lấy hình ảnh từ camera')