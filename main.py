import json
import math
import os
from queue import Queue
import random
import threading
import time
import numpy as np

import cv2
import keyboard
import pygame
from scipy.spatial.distance import cdist
from CameraImouLife.RTSP import VideoThread
from CameraImouLife.detect import DetectionThread
from HybridNets.detect_model import HybridInference
from HybridNets.make_labels import MakeLabel
# import keyboard
from MQTT.Publisher.publisher import Publisher
from MQTT.Subscriber.subscriber import Subscriber


class Main:
    def __init__(self, result_path):
        self.broker_ip = "192.168.1.199"
        self.username = "son1711"
        self.password = "sonk51ca1"
        self.left = 0
        self.right = 0 
        self.sensor1 = 0
        self.sensor2 = 0
        self.sensor3 = 0
        self.results = []
        self.distances = []
        self.result_path = result_path
        self.left = 0
        self.right = 0
        self.turn_left = 0
        self.turn_right = 0
        

        self.exit_event = threading.Event()

    def run_publisher(self):
        publisher = Publisher(self.broker_ip, self.username, self.password)
        topic1 = "left"
        topic2 = "right"
        while True:
          
            
            publisher.publish_message(topic1, str(self.left))
            publisher.publish_message(topic2, str(self.right))
            time.sleep(0.5)

    
    def run_subscriber(self):
        subscriber = Subscriber(self.broker_ip, self.username, self.password,self)
        # topic = "topic"
        subscriber.client.loop_forever()
        # subscriber.disconnect()  # Chú ý: Đoạn này sẽ chạy vô hạn, bạn có thể gọi hàm disconnect() để ngắt kết nối

    def _preprocess(self, input):
        if isinstance(input, str):
           img = cv2.imread(input, cv2.IMREAD_COLOR | cv2.IMREAD_IGNORE_ORIENTATION)
        elif isinstance(input, np.ndarray):
            img = input.copy()
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        h0, w0 = img.shape[:2]
        r = RESIZED_SHAPE / max(img.shape[:2])
        input_img = (cv2.resizeimg, (int(w0 * r), int(h0 * r)), interpolation=cv2.INTER_AREA)
        (input_img, _), _, _ = letterbox(
            (input_img, None), RESIZED_SHAPE, auto=True, scaleup=False
        )
        input_img = self.transform(input_img)
        input_img = input_img.unsqueeze(0)
        input_img.to(self.device)
        return input_img.to(torch.float32)


    def get_param_from_hardware(self):
            
        speed = {
            'left': 500, #self.left,
            'right': 500 #self.right
        }
        ultrasonic = {
            'sonic_1': random.randint(150, 200),#self.sensor1,
            'sonic_2': random.randint(450, 500),#self.sensor2,
            'sonic_3': random.randint(200, 300)#self.sensor3
        }
        return speed, ultrasonic
    def handle_key_hold(self,event):
        if event.name == "a":
            self.left += 5
            print("left =", self.left)
            print("right =", self.right)
        if  event.name == "d":
            self.right += 5
            print("left =", self.left)
            print("right =", self.right)
        if  event.name == "s":
            self.left += 5
            self.right += 5
            print("left =", self.left)
            print("right =", self.right)
        if  event.name == "w":
            self.left -= 5
            self.right -= 5
            print("left =", self.left)
            print("right =", self.right)
        if  event.name == "f":
            self.left = 5
            self.right = 5
            print("left =", self.left)
            print("right =", self.right)

    def control_pygame(self):
        keyboard.on_press(self.handle_key_hold)
        while True:
            time.sleep(0.3)
            


    def run_video(self):
        id_camera =  0#'D:/dev/New folder/HybridNets/demo/video/1.mp4'
        output_path = 'D:/dev/New folder/HybridNets/demo/video/test.mp4'
        result_path = 'D:/dev/New folder/HybridNets/demo/result_template.json'
        
        results = []
        try:
            with open(result_path, 'r') as f:
                results = json.load(f)
        except:
            results = []

        detect = HybridInference()

        image_queue = Queue()  # Tạo hàng đợi để chia sẻ hình ảnh
        speed, ultrasonic = self.get_param_from_hardware()
        video_thread = VideoThread(id_camera, image_queue)
        detection_thread = DetectionThread(speed, ultrasonic, results, image_queue)

        print("Running your camera ...")
        
        # Khởi chạy 2 luồng
        video_thread.start()
        detection_thread.start()

        # Đợi cho tới khi 2 luồng hoàn thành
        video_thread.join()
        detection_thread.join()

        # Lưu kết quả vào file
        with open(result_path, 'w') as f:
            json.dump(results, f, indent=4)

        # print(f"Overall dataset: [{detection_thread.count}]")
        # print(f"Completed labeling and wrote data to [{result_path}]")
        
if __name__ == "__main__":
    result_dir = 'HybridNets\demo_result\data_result'
    out_name = 'result_template.json'
    result_path = os.path.join(result_dir, out_name)
    main = Main(result_path)
    
    exit_event = threading.Event()
    publisher_thread = threading.Thread(target=main.run_publisher)
    subscriber_thread = threading.Thread(target=main.run_subscriber)
    
    runvideo_thead = threading.Thread(target=main.run_video)
    control_thead = threading.Thread(target=main.control_pygame)
    
    publisher_thread.start()
    subscriber_thread.start()
    runvideo_thead.start()
    control_thead.start()

    publisher_thread.join()
    subscriber_thread.join()
    runvideo_thead.join()
    control_thead.join()