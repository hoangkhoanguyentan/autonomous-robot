import paho.mqtt.client as mqtt

class Publisher:
    def __init__(self, broker_ip, username, password):
        self.client = mqtt.Client()
        self.client.username_pw_set(username, password)
        self.client.connect(broker_ip, 1883, 60)
    
    def publish_message(self, topic, message):
        self.client.publish(topic, message)
    
    def disconnect(self):
        self.client.disconnect()