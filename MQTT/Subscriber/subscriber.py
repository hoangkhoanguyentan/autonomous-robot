import threading
import paho.mqtt.client as mqtt

class Subscriber:
    def __init__(self, broker_ip, username, password, main_instance):
        self.client = mqtt.Client()
        self.client.username_pw_set(username, password)
        self.client.connect(broker_ip, 1883, 60)
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        self.sensor1 = 0
        self.sensor2 = 0
        self.sensor3 = 0
        self.main_instance = main_instance
        self.lock = threading.Lock()
    
    def on_connect(self, client, userdata, flags, rc):
        if rc == 0:
            print("Kết nối thành công!")
            client.subscribe("sensor1")
            client.subscribe("sensor2")
            client.subscribe("sensor3")
        else:
            print("Kết nối không thành công. Mã lỗi:", rc)
    
    def on_message(self, client, userdata, msg):
        with self.lock:
            if msg.topic == "sensor1":
                    self.main_instance.sensor1 = msg.payload.decode()
                    
            if msg.topic == "sensor2":
                    self.main_instance.sensor2 = msg.payload.decode()
           
            if msg.topic == "sensor3":
                    self.main_instance.sensor3 = msg.payload.decode()
    
    def subscribe_topic(self, topic):
        self.client.subscribe(topic)
        self.client.loop_forever()
    
    def disconnect(self):
        self.client.disconnect()