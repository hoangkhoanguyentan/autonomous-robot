import json
import queue
import numpy as np
from sklearn.ensemble import RandomForestRegressor

import json
import os
import threading
import time
import numpy as np

import cv2
import keyboard
import pygame
from CameraImouLife.Detection_live_Thread import Detection_live_Thread
from CameraImouLife.RTSP import VideoThread
from CameraImouLife.detect import DetectionThread
from HybridNets.detect_model import HybridInference
from HybridNets.make_labels import MakeLabel
from MQTT.Publisher.publisher import Publisher
from MQTT.Subscriber.subscriber import Subscriber


class Main:
    def __init__(self, result_path):
        self.broker_ip = "192.168.1.199"
        self.username = "son1711"
        self.password = "sonk51ca1"
        self.left = 0
        self.right = 0 
        self.sensor1 = 0
        self.sensor2 = 0
        self.sensor3 = 0
        self.results = []
        self.result_path = result_path
        self.left = 0
        self.right = 0
        self.turn_left = 0
        self.turn_right = 0

        self.exit_event = threading.Event()

    def run_publisher(self):
        publisher = Publisher(self.broker_ip, self.username, self.password)
        topic1 = "left"
        topic2 = "right"
        while True:
          
            publisher.publish_message(topic1, str(self.left))
            publisher.publish_message(topic2, str(self.right))
            time.sleep(0.5)

    
    def run_subscriber(self):
        subscriber = Subscriber(self.broker_ip, self.username, self.password,self)
        # topic = "topic"
        subscriber.client.loop_forever()
        # subscriber.disconnect()  # Chú ý: Đoạn này sẽ chạy vô hạn, bạn có thể gọi hàm disconnect() để ngắt kết nối

    def get_param_from_hardware(self):
            
        speed = [
            self.left,
            self.right
        ]
        ultrasonic = [
            self.sensor1,
            self.sensor2,
            self.sensor3
            
        ]
        return speed, ultrasonic
    def control_Ultra(self):
        if self.sensor2 <= 100:
            self.left = 1
            self.right = 1

            
            
    def define_vector(self,img):
        contours, _ = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        canvas = np.copy(img)    
        for contour in contours:
            # Xấp xỉ đường viền bằng đa giác
            epsilon = 0.02 * cv2.arcLength(contour, True)
            approx = cv2.approxPolyDP(contour, epsilon, True)

            # Lấy tọa độ các điểm trong đa giác
            points = approx[:, 0, :]

            # Tọa độ điểm bắt đầu (góc dưới của khung ảnh)
            start_point = (img.shape[1]//2, img.shape[0])

            # Vẽ vector từ điểm bắt đầu đến các góc của đa giác
            for point in points:
                angle = np.arctan2(point[1] - start_point[1], point[0] - start_point[0]) * 180 / np.pi
                if (-150 <= angle <= -35):
                    for fixed_angle in [-40, -70, -80, -145]:
                        rad = np.radians(fixed_angle)
                        end_point_x = start_point[0] + int(np.cos(rad) * img.shape[1])
                        end_point_y = start_point[1] + int(np.sin(rad) * img.shape[1])
                        cv2.arrowedLine(canvas, start_point, (end_point_x, end_point_y), (0, 0, 200), 2)
                    cv2.arrowedLine(canvas, start_point, tuple(point), (0, 0, 200), 2)

            cv2.imshow("Labeling dataset", canvas)

        



        # id_camera = 0
        # #'rtsp://admin:sonk51ca1@192.168.1.171:554/cam/realmonitor?channel=1&subtype=1'
        # make_label = MakeLabel(result_dir, out_name, id_camera)
        # make_label.run_video()
if __name__ == "__main__":
    id_camera =  0#'rtsp://admin:L2B274DC@192.168.1.171:554/cam/realmonitor?channel=1&subtype=1'
 
    result_dir = 'HybridNets\demo_result\data_result'
    out_name = 'result_template.json'
    result_path = os.path.join(result_dir, out_name)
    main = Main(result_path)
    speed, ultrasonic = main.get_param_from_hardware()
    exit_event = threading.Event()
    publisher_thread = threading.Thread(target=main.run_publisher)
    subscriber_thread = threading.Thread(target=main.run_subscriber)
    
    image_queue = queue.Queue()
    video_thread = VideoThread(id_camera, image_queue)
    
    detection_thread = Detection_live_Thread(ultrasonic,image_queue, main)

    # control_thead = threading.Thread(target=main.control_pygame)
    
    publisher_thread.start()
    subscriber_thread.start()
    video_thread.start()
    detection_thread.start()
    # control_thead.start()

    publisher_thread.join()
    subscriber_thread.join()
    video_thread.join()
    detection_thread.join()
    # control_thead.join()
    
# Đọc dữ liệu từ file JSON

# Dữ liệu mới
