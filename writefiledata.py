
from sklearn.ensemble import RandomForestRegressor

from CameraImouLife.SpeedPredictor import SpeedPredictor_RdForest

new_X = ["146.81909178259053", "99.57820605163424", "460.9806018581385", "189.49340837388806", "85.73128089125954", "383.1191163446187", "29.350451922996832", "161.0200824839635", "618.2264826464252", "281.02673338543445", "163.32471044175998", "0.912439619767666"]
speed_predictor = SpeedPredictor_RdForest()
speed_predictor.load_model("HybridNets/demo_result/data_result/peed_predictor_model_1.joblib")

predicted_speed_left, predicted_speed_right = speed_predictor.predict(new_X)

left = predicted_speed_left
right =predicted_speed_right

print(left)
print(right)