import RPi.GPIO as GPIO
import time

class UltrasonicSensor:
    def __init__(self, trig_pin, echo_pin, ):
        self.trig_pin = trig_pin
        self.echo_pin = echo_pin
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.trig_pin, GPIO.OUT)
        GPIO.setup(self.echo_pin, GPIO.IN)
        

    def measure_distance(self):
        # Tạo xung trigger
        GPIO.output(self.trig_pin, GPIO.LOW)
        time.sleep(0.2)
        GPIO.output(self.trig_pin, GPIO.HIGH)
        time.sleep(0.00001)
        GPIO.output(self.trig_pin, GPIO.LOW)

        # Đo thời gian từ khi phát xung trigger đến khi nhận được xung echo
        pulse_start = 0
        pulse_end = 0
        while GPIO.input(self.echo_pin) == GPIO.LOW:
            pulse_start = time.time()
        while GPIO.input(self.echo_pin) == GPIO.HIGH:
            pulse_end = time.time()

        # Tính khoảng cách dựa trên thời gian
        pulse_duration = pulse_end - pulse_start
        distance = pulse_duration * 34300 / 2

        return distance

    def cleanup(self):
        GPIO.cleanup()