import RPi.GPIO as GPIO


class DM556:
    def __init__(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        # Right motor
        GPIO.setup(18, GPIO.OUT)  # Pulse signal
        GPIO.setup(23, GPIO.OUT)  # Pulse signal
        GPIO.setup(24, GPIO.OUT)  # Enable driver
        # Left motor
        GPIO.setup(25, GPIO.OUT)  # Pulse signal
        GPIO.setup(8, GPIO.OUT)  # Pulse signal
        GPIO.setup(7, GPIO.OUT)  # Enable driver

        GPIO.output(23, GPIO.LOW)
        GPIO.output(24, GPIO.LOW)
        GPIO.output(8, GPIO.LOW)
        GPIO.output(7, GPIO.LOW)

        self.left = GPIO.PWM(25, 1)
        self.right = GPIO.PWM(18, 1)

    def control_motors(self, a, b):
        
        if a == 0:
            GPIO.output(24, GPIO.HIGH)
            self.right.stop()
        elif float(a) < 0:
            GPIO.output(23, GPIO.HIGH)
            self.right.ChangeFrequency(-float(a))
            self.right.start(50)
        elif float(a) > 0:
            GPIO.output(23, GPIO.LOW)
            self.right.ChangeFrequency(float(a))
            self.right.start(50)

        if b == 0:
            GPIO.output(7, GPIO.HIGH)
            self.left.stop()
        elif float(b) < 0:
            GPIO.output(8, GPIO.LOW)
            self.left.ChangeFrequency(-float(b))
            self.left.start(50)
        elif float(b) > 0:
            GPIO.output(8, GPIO.HIGH)
            self.left.ChangeFrequency(float(b))
            self.left.start(50)
            
# dm556 = DM556()        
# while True:
#     a = input("Enter the value for motor A: ")
#     b = input("Enter the value for motor B: ")
#     dm556.control_motors(float(a), float(b))
#     if a == 'q':
#         break

