
import threading
import paho.mqtt.client as mqtt
from driver.DM556 import DM556

class Subscriber:
    def __init__(self, broker_ip, username, password):
        self.client = mqtt.Client()
        self.client.username_pw_set(username, password)
        self.client.connect(broker_ip, 1883, 60)
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        self.left = None
        self.right = None
        self.message_count = 0
        self.status = True
        self.ctr = 0
        self.lock = threading.Lock()
        
        self.control_thread = None
        self.subscribe_thread = None

    def start(self):
        self.subscribe_thread = threading.Thread(target=self.subscribe_topic)
        self.subscribe_thread.start()

    def stop(self):
        self.status = False
        self.subscribe_thread.join()
        if self.control_thread is not None:
            self.control_thread.stop()
            self.control_thread.join()

    def on_connect(self, client, userdata, flags, rc):
        if rc == 0:
            print("Kết nối thành công!")
            client.subscribe("left")
            client.subscribe("right")
        else:
            print("Kết nối không thành công. Mã lỗi:", rc)

    def on_message(self, client, userdata, msg):
        
        if msg.topic == "left":
            with self.lock:
                self.left = msg.payload.decode()
                self.message_count += 1
                

        if msg.topic == "right":
            with self.lock:
                self.right = msg.payload.decode()
                self.message_count += 1
                print(self.right)
        if(self.message_count % 2 == 0):
            if self.control_thread is None:
                self.start_control_thread()

    def subscribe_topic(self):
        self.client.loop_forever()

    def start_control_thread(self):
        if self.control_thread is not None:
            self.control_thread.stop()
            self.control_thread.join()

        self.control_thread = threading.Thread(target=self.control_Driver)
        self.control_thread.start()

    def control_Driver(self):
        print(self.status)
        print(self.left)
        print(self.right)
        dm556 = DM556()
        while self.status:
            with self.lock:
                left = self.left
                right = self.right

                dm556.control_motors(left, right)



