import threading
import time


from Publisher.Publisher import Publisher
from Subscriber.Subscriber import Subscriber
from Module.UltrasonicSensor import UltrasonicSensor


class Main:
    def __init__(self):
        self.broker_ip = "192.168.1.199"
        self.username = "son1711"
        self.password = "sonk51ca1"
        self.publisher = Publisher(self.broker_ip, self.username, self.password)
        self.sensor1 = UltrasonicSensor(trig_pin=16, echo_pin=26)
        self.sensor2 = UltrasonicSensor(trig_pin=5, echo_pin=6)
        self.sensor3 = UltrasonicSensor(trig_pin=17, echo_pin=27)
    def run_publisher(self):
        topic1 = "sensor1"
        topic2 = "sensor2"
        topic3 = "sensor3"
        
        while True:
            # Đọc giá trị từ 3 cảm biến siêu âm
            distance_sensor1 = self.sensor1.measure_distance()
            distance_sensor2 = self.sensor2.measure_distance()
            distance_sensor3 = self.sensor3.measure_distance()
            
            # Gửi thông điệp đến MQTT broker cho từng cảm biến
            self.publisher.publish_message(topic1, str(distance_sensor1))
            self.publisher.publish_message(topic2, str(distance_sensor2))
            self.publisher.publish_message(topic3, str(distance_sensor3))
            
            # Đợi một khoảng thời gian trước khi gửi thông điệp tiếp theo
            time.sleep(1)

    def run_subscriber(self):
        subscriber = Subscriber(self.broker_ip, self.username, self.password)
        
        subscriber.client.loop_forever()
        # subscriber.disconnect()  # Chú ý: Đoạn này sẽ chạy vô hạn, bạn có thể gọi hàm disconnect() để ngắt kết nối

if __name__ == "__main__":
    main = Main()
    exit_event = threading.Event()
    publisher_thread = threading.Thread(target=main.run_publisher)
    subscriber_thread = threading.Thread(target=main.run_subscriber)
    
    publisher_thread.start()
    subscriber_thread.start()

    publisher_thread.join()
    subscriber_thread.join()